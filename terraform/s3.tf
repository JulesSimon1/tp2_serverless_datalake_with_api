# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket_tp_esme_3_julessimon99" {
  bucket = "s3-job-offer-bucket-tp-esme-3-julessimon99"
  force_destroy = true

  tags = {
    Name = "Job Offer Bucket"
  }
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_tp_esme_3_julessimon99.id
  key = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "lambda_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_tp_esme_3_julessimon99.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.job_processing_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
